package de.grogra.video.connector;

import java.util.List;

import de.grogra.video.render.ImageProvider;
import de.grogra.video.simulation.SimulationMethod;

/**
 * This interface connects the plugin the the rest of the GroIMP implementation.
 * There are methods to get all available {@link ImageProvider}s and
 * {@link SimulationMethod}s. Thats all the plugin needs to know in order to
 * communicate with GroIMP.
 * 
 * @author Dominick Leppich
 *
 */
public interface VideoPluginConnector {
	/**
	 * Get a list of available {@link ImageProvider}s.
	 * 
	 * @return List of {@link ImageProvider}s
	 */
	List<ImageProvider> getImageProviders();

	/**
	 * Get a list of available {@link SimulationMethod}s.
	 * 
	 * @return List of {@link SimulationMethod}s
	 */
	List<SimulationMethod> getSimulationMethods();
}
