package de.grogra.video.model;

/**
 * Implements an {@link ImageSequenceControl} which grants changing access to
 * the sequence given by objects creation by simply passing all method calls to
 * the sequence.
 * 
 * @author Dominick Leppich
 *
 */
public class ImageSequenceController implements ImageSequenceControl {
	private ImageSequence imageSequence;

	// ------------------------------------------------------------
	
	ImageSequenceController(ImageSequence imageSequence) {
		this.imageSequence = imageSequence;
	}

	@Override
	public void add(VideoImage image) {
		imageSequence.add(image);
	}

	@Override
	public void add(int index, VideoImage image) {
		imageSequence.add(index, image);
	}

	@Override
	public void clear() {
		imageSequence.clear();
	}
	
	@Override
	public VideoImage remove(int index) {
		return imageSequence.remove(index);
	}
}
