package de.grogra.video.util;

import java.io.IOException;

/**
 * This abstract class defines a {@code Job} for the {@link Worker} class, which
 * is {@link ProgressObservable}.
 * 
 * @author Dominick Leppich
 *
 */
public abstract class Job extends ProgressObservable {
	/**
	 * This {@link #process()} method defines the work, the {@code Job} consists of.
	 * 
	 * @throws IOException
	 *             if an {@link IOException} occurs during job processing
	 * @throws InterruptedException
	 *             if the {@code Job} is interrupted
	 */
	public abstract void process() throws IOException, InterruptedException;
}
