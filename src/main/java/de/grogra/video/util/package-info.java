/**
 * This package contains utility classes which are used across the plugin.
 * 
 * @author Dominick Leppich
 * 
 */
package de.grogra.video.util;