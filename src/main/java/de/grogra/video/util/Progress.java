package de.grogra.video.util;

/**
 * A {@code Progress} defines a "progress", it's really that simple. Each
 * progress has a name and a value. Name and value of a progress can't be edited
 * after creation.
 * 
 * @author Dominick Leppich
 *
 */
public class Progress {
	private final double value;
	private final String name;

	// ------------------------------------------------------------

	/**
	 * Create a new {@code Progress} object with a value and a name.
	 * 
	 * @param value
	 *            - Value of the progress (should be between 0.0 and 1.0)
	 * @param name
	 *            - Name of the progress (a description)
	 */
	public Progress(double value, String name) {
		this.value = value;
		this.name = name;
	}

	// ------------------------------------------------------------

	/**
	 * Get the value of the progress.
	 * 
	 * @return Value
	 */
	public double getValue() {
		return value;
	}

	/**
	 * Get the name of the progress.
	 * 
	 * @return Name
	 */
	public String getName() {
		return name;
	}
}
