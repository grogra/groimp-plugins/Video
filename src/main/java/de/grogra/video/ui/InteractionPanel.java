package de.grogra.video.ui;

/**
 * This interface can be used to enable or disable the input on a plugin panel.
 * 
 * @author Dominick Leppich
 *
 */
public interface InteractionPanel {
	/**
	 * Enable or disable the interaction with this panel.
	 * 
	 * @param enabled
	 *            - New status
	 */
	void setInteractionEnabled(boolean enabled);
}
