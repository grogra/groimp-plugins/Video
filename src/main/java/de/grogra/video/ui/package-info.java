/**
 * This package contains all graphical user interface classes of the plugin.
 * 
 * @author Dominick Leppich
 * 
 */
package de.grogra.video.ui;