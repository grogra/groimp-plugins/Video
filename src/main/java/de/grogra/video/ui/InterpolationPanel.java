package de.grogra.video.ui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import de.grogra.video.VideoPlugin;
import de.grogra.video.interpolation.InterpolationJob;
import de.grogra.video.interpolation.InterpolationStrategy;
import de.grogra.video.model.ImageSequence;
import de.grogra.video.model.ImageSequenceControl;
import de.grogra.video.model.ImageSequenceEvent;
import de.grogra.video.model.ImageSequenceView;
import de.grogra.video.util.Worker;

/**
 * This panel controls the image generation process
 * 
 * @author Dominick Leppich
 *
 */
public class InterpolationPanel extends JPanel implements Observer, InteractionPanel {
	private static final long serialVersionUID = 1L;

	private Worker worker;
	private ImageSequenceView view;
	private ImageSequenceControl control;

	private boolean inputEnabled;

	private JComboBox<InterpolationStrategy> imageGenerators;
	private SpinnerNumberModel startIndexModel;
	private SpinnerNumberModel endIndexModel;
	private SpinnerNumberModel stepModel;
	private JButton interpolateButton;

	private Set<JComponent> components;

	/**
	 * Create the {@code InterpolationPanel} and position all user interface
	 * elements on the panel.
	 * 
	 * @param worker
	 *            - {@link Worker} to use for the export
	 * @param view
	 *            - {@link ImageSequenceView} to get reading access to the
	 *            {@link ImageSequence}
	 * @param control-
	 *            {@link ImageSequenceControl} to get writing access to the
	 *            {@link ImageSequence}
	 */
	public InterpolationPanel(Worker worker, ImageSequenceView view, ImageSequenceControl control) {
		/*
		 * Creating swing objects and positioning them on the panel using some layout as
		 * well as adding some simple listeners to them won't be explained in detail.
		 */
		this.worker = worker;
		this.view = view;
		this.control = control;

		components = new HashSet<>();

		setBorder(BorderFactory.createTitledBorder("Image interpolation"));

		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JLabel strategyLabel = new JLabel("Strategy:");
		components.add(strategyLabel);
		add(strategyLabel);
		imageGenerators = createGeneratorSelectionBox();
		components.add(imageGenerators);
		add(imageGenerators);
		JLabel startLabel = new JLabel("Start:");
		components.add(startLabel);
		add(startLabel);
		startIndexModel = new SpinnerNumberModel(0, 0, 99999, 1);
		startIndexModel.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				int startValue = startIndexModel.getNumber().intValue();
				int endValue = endIndexModel.getNumber().intValue();

				if (startValue == 0 || endValue == 0)
					return;

				if (startValue >= endValue)
					startIndexModel.setValue(endValue - 1);
			}
		});
		JSpinner startIndexSpinner = new JSpinner(startIndexModel);
		components.add(startIndexSpinner);
		add(startIndexSpinner);
		JLabel endLabel = new JLabel("End:");
		components.add(endLabel);
		add(endLabel);
		endIndexModel = new SpinnerNumberModel(0, 0, 99999, 1);
		endIndexModel.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				int startValue = startIndexModel.getNumber().intValue();
				int endValue = endIndexModel.getNumber().intValue();

				if (startValue == 0 || endValue == 0)
					return;

				if (endValue <= startValue)
					endIndexModel.setValue(startValue + 1);
			}
		});
		JSpinner endIndexSpinner = new JSpinner(endIndexModel);
		components.add(endIndexSpinner);
		add(endIndexSpinner);
		JLabel stepsLabel = new JLabel("Steps:");
		components.add(stepsLabel);
		add(stepsLabel);
		stepModel = new SpinnerNumberModel(5, 1, 99999, 1);
		stepModel.addChangeListener(new ChangeModelListener(stepModel));
		JSpinner stepSpinner = new JSpinner(stepModel);
		components.add(stepSpinner);
		add(stepSpinner);
		interpolateButton = new JButton("Interpolate");
		interpolateButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				applyImageGeneratorStrategy();
			}
		});
		components.add(interpolateButton);
		add(interpolateButton);

		// You cannot interpolate without images
		inputEnabled = false;
		setInteractionEnabled(inputEnabled);
	}

	/**
	 * Create a {@link JComboBox} containing all available
	 * {@link InterpolationStrategy} instances.
	 * 
	 * @return
	 */
	private JComboBox<InterpolationStrategy> createGeneratorSelectionBox() {
		List<InterpolationStrategy> generatorList = VideoPlugin.getInterpolationStrategies();
		InterpolationStrategy[] generators = new InterpolationStrategy[generatorList.size()];
		generatorList.toArray(generators);
		JComboBox<InterpolationStrategy> generatorBox = new JComboBox<>(generators);
		generatorBox.setSelectedIndex(0);
		return generatorBox;
	}

	/**
	 * Generate interpolated images by creating a {@link InterpolationJob} and pass
	 * it to the {@link Worker}.
	 */
	public void applyImageGeneratorStrategy() {
		worker.addJob(new InterpolationJob(imageGenerators.getItemAt(imageGenerators.getSelectedIndex()), view, control,
				stepModel.getNumber().intValue(), startIndexModel.getNumber().intValue() - 1,
				endIndexModel.getNumber().intValue() - 1));
	}

	@Override
	public void setInteractionEnabled(boolean enabled) {
		inputEnabled = enabled;
		boolean value = inputEnabled && view.size() >= 2;
		this.setEnabled(value);
		for (JComponent c : components)
			c.setEnabled(value);
	}

	/**
	 * Update the spinners when the {@link ImageSequence} changes.
	 */
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof ImageSequence && arg instanceof ImageSequenceEvent) {
			ImageSequenceEvent event = (ImageSequenceEvent) arg;

			// Update Start and end index models
			final int startMin = view.size() < 2 ? 0 : 1;
			final int startMax = view.size() < 2 ? 0 : view.size() - 1;
			final int endMin = view.size() < 2 ? 0 : 2;
			final int endMax = view.size() < 2 ? 0 : view.size();
			final int currentStartValue = startIndexModel.getNumber().intValue();
			final int currentEndValue = endIndexModel.getNumber().intValue();

			// Set values
			int newStartValue = 0;
			int newEndValue = 0;

			// Set value if needed
			if (view.size() >= 2) {
				if (currentStartValue < startMin)
					newStartValue = startMin;
				else if (currentStartValue > startMax)
					newStartValue = startMax;
				else
					newStartValue = currentStartValue;

				if (currentEndValue < endMin)
					newEndValue = endMin;
				else if (currentEndValue > endMax)
					newEndValue = endMax;
				// Grow with sequence if full sequence was set
				else if (event.getType() == ImageSequenceEvent.IMAGE_ADDED && currentStartValue == 1
						&& currentEndValue == endMax - 1)
					newEndValue = endMax;
				else
					newEndValue = currentEndValue;
			}

			startIndexModel.setMinimum(startMin);
			startIndexModel.setMaximum(startMax);
			endIndexModel.setMinimum(endMin);
			endIndexModel.setMaximum(endMax);
			startIndexModel.setValue(newStartValue);
			endIndexModel.setValue(newEndValue);

			// Disable components if not useable
			setInteractionEnabled(inputEnabled);
		}
	}
}
