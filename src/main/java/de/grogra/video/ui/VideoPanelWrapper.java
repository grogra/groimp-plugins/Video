package de.grogra.video.ui;

import de.grogra.pf.ui.swing.PanelSupport;
import de.grogra.pf.ui.swing.SwingPanel;

/**
 * This class wraps the {@link VideoPanel} (graphical user interface of the
 * plugin) into a panel, which is compatible with GroIMP. In this case the
 * GroIMP class {@link PanelSupport} is used.
 * 
 * @author Dominick Leppich
 *
 */
public class VideoPanelWrapper extends PanelSupport {
	private VideoPanel videoPanel;

	// ------------------------------------------------------------

	/**
	 * Default ctr.
	 */
	public VideoPanelWrapper(VideoPanel videoPanel) {
		super(new SwingPanel(null));

		this.videoPanel = videoPanel;

		((SwingPanel) getComponent()).getContentPane().add(videoPanel);
	}

	/**
	 * Pass the dispose call to the {@link VideoPanel}.
	 */
	@Override
	public void dispose() {
		super.dispose();

		videoPanel.dispose();
	}
}
