package de.grogra.video.test;

/**
 * The {@code TimeStopper} can be used to measure time. To program lifetime
 * there can only be one instance of this class, which can be created using the
 * {@link #instance()} method. To stop the time you just have to call the
 * methods {@link #start()} and {@link #stop()} to start or stop the
 * measurement. To print the results, just print the {@code TimeStopper} object
 * or directly call the {@link #toString()} method. The output format can be set
 * using the {@link #setFormat(Format)} method.
 * 
 * @author Dominick Leppich
 *
 */
public class TimeStopper {
	/**
	 * Enumeration of available output formats of the {@link TimeStopper}.
	 * 
	 * @author Dominick Leppich
	 *
	 */
	public enum Format {
		RAW, HUMAN_READABLE;
	}

	// ------------------------------------------------------------

	private static TimeStopper instance;

	private long start;
	private long end;

	private Format format = Format.HUMAN_READABLE;

	// ------------------------------------------------------------

	/**
	 * Ctr. This empty private construktor is needed in order to hide the
	 * constructor to the outside.
	 */
	private TimeStopper() {

	}

	/**
	 * Get (and create on first call) an instance of the {@link TimeStopper} class.
	 * 
	 * @return A {@link TimeStopper} instance
	 */
	public static synchronized TimeStopper instance() {
		if (instance == null)
			instance = new TimeStopper();
		return instance;
	}

	// ------------------------------------------------------------

	/**
	 * Set the output format of the monitor.
	 * 
	 * @param format
	 *            - Output format as {@link Format} enumeration
	 */
	public void setFormat(Format format) {
		this.format = format;
	}

	// ------------------------------------------------------------

	/**
	 * Start measuring time.
	 */
	public void start() {
		start = System.nanoTime();
	}

	/**
	 * Stop measuring time.
	 */
	public void stop() {
		end = System.nanoTime();
	}

	// ------------------------------------------------------------

	/**
	 * Get a string representation of the last measured result in the defined
	 * format.
	 * 
	 * @return Result of the last measurement
	 */
	public String toString() {
		if (format == Format.HUMAN_READABLE)
			return "Time measured: " + String.format("%.4f", (double) (end - start) / 1000000000) + " s";
		else
			return String.format("%.4f", (double) (end - start) / 1000000000);
	}
}
