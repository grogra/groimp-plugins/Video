package de.grogra.video.interpolation;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import de.grogra.video.model.VideoImage;
import de.grogra.video.util.Progress;

/**
 * This abstract strategy computes {@code n} images between two
 * {@link VideoImage}s {@code A} and {@code B}. The computation logic between
 * these two images has to be implemented in the abstract
 * {@link #computeImages(VideoImage, VideoImage, int)} method.
 * 
 * @author Dominick Leppich
 *
 */
public abstract class SimpleInterpolationStrategy extends InterpolationStrategy {
	private double mainProgress;
	private double subProgressFrac;
	private double subProgress;

	// ------------------------------------------------------------

	@Override
	protected final List<VideoImage> computeImages(List<VideoImage> images, int n) throws IOException {
		List<VideoImage> result = new LinkedList<>();

		// Generate n images between each image pair in the sequence
		int size = images.size();

		// Calculate progress denominator
		subProgressFrac = (double) 1 / (size - 1);

		mainProgress = 0.0;
		updateProgress();

		for (int i = 0; i < size - 1; i++) {
			result.addAll(computeImages(images.get(i), images.get(i + 1), n));
			mainProgress = (double) (i + 1) / (size - 1);
		}

		return result;
	}

	/**
	 * Compute {@code n} images between the images {@code A} and {@code B}. In order
	 * to calculate the progress of this strategy correctly, each subclass needs to
	 * call the method {@link #setSubProgress(double)} after the computation of each
	 * image inside this method.
	 * 
	 * @param a
	 *            - Image A
	 * @param b
	 *            - Image B
	 * @param n
	 *            - Number of images to compute between A and B
	 * @return A list of computed images
	 * @throws IOException
	 *             if the computation fails
	 */
	protected abstract List<VideoImage> computeImages(VideoImage a, VideoImage b, int n) throws IOException;

	// ------------------------------------------------------------

	/**
	 * Set the progress in the computation between two images.
	 * 
	 * @param subProgress
	 *            - Progress as a double value (0,1)
	 */
	protected final void setSubProgress(double subProgress) {
		this.subProgress = subProgress;
		updateProgress();
	}

	/**
	 * Updates the progress of this {@link SimpleInterpolationStrategy}.
	 */
	private void updateProgress() {
		setProgress(new Progress(mainProgress + subProgressFrac * subProgress, "Generating Images: " + getName()));
	}
}
