package de.grogra.video.interpolation;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import de.grogra.video.model.ImageSequence;
import de.grogra.video.model.ImageSequenceControl;
import de.grogra.video.model.ImageSequenceView;
import de.grogra.video.model.VideoImage;
import de.grogra.video.util.Job;
import de.grogra.video.util.Progress;
import de.grogra.video.util.Worker;

/**
 * The {@code InterpolationJob} encapsulates the interpolation of images into a
 * {@link Job} in order to be processed by a {@link Worker}.
 * 
 * @author Dominick Leppich
 *
 */
public class InterpolationJob extends Job implements Observer {
	private InterpolationStrategy strategy;
	private ImageSequenceView view;
	private ImageSequenceControl control;
	private int count;
	private int startIndex;
	private int endIndex;

	// ------------------------------------------------------------

	/**
	 * Create a new {@link InterpolationJob} defining the
	 * {@link InterpolationStrategy}, providing {@link ImageSequenceView} and
	 * {@link ImageSequenceControl} to the {@link ImageSequence}, the number of
	 * images to compute between two images and the start and end index of the
	 * sequence where the interpolation should be applied.
	 * 
	 * @param strategy
	 *            - {@link InterpolationStrategy}
	 * @param view
	 *            - {@link ImageSequenceView}
	 * @param control
	 *            - {@link ImageSequenceControl}
	 * @param count
	 *            - Number of images to compute between two images
	 * @param startIndex
	 *            - Index of the unmodified {@link ImageSequence} where the
	 *            interpolation should start
	 * @param endIndex-
	 *            Index of the unmodified {@link ImageSequence} where the
	 *            interpolation should end
	 */
	public InterpolationJob(InterpolationStrategy strategy, ImageSequenceView view, ImageSequenceControl control,
			int count, int startIndex, int endIndex) {
		this.strategy = strategy;
		this.view = view;
		this.control = control;
		this.count = count;
		this.startIndex = startIndex;
		this.endIndex = endIndex;
	}

	@Override
	public void process() throws IOException {
		// Check for correct start and end
		if (startIndex < 0 || startIndex > view.size() - 2)
			throw new IndexOutOfBoundsException("start index out of bounds: " + startIndex);
		if (endIndex < 1 || endIndex > view.size() - 1)
			throw new IndexOutOfBoundsException("end index out of bounds: " + startIndex);
		if (startIndex >= endIndex)
			throw new IllegalArgumentException(
					"start index not smaller than end index: " + startIndex + " >= " + endIndex);

		strategy.addObserver(this);

		// Create a sublist
		List<VideoImage> imagesToProcess = new LinkedList<>();
		for (int i = startIndex; i <= endIndex; i++)
			imagesToProcess.add(view.get(i));

		List<VideoImage> generatedImages = strategy.computeImages(imagesToProcess, count);

		int index = startIndex;
		for (int i = 0; i < generatedImages.size(); i++) {
			// Skip existing image
			if (i % count == 0)
				index++;

			// index++;
			control.add(index++, generatedImages.get(i));
		}

		strategy.deleteObserver(this);
	}

	// ------------------------------------------------------------

	/**
	 * Pass the progress to the {@link Job} class.
	 */
	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg0 instanceof InterpolationStrategy && arg1 instanceof Progress) {
			Progress p = (Progress) arg1;
			setProgress(p);
		}
	}
}
