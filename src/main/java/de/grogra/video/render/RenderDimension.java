package de.grogra.video.render;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

/**
 * Dimension used for rendering purpose. The Java {@link Dimension} class was
 * extended in order to have a user interface friendly String representation and
 * to have additional information about the aspect ratio.
 * 
 * @author Dominick Leppich
 *
 */
public class RenderDimension extends Dimension {
	/**
	 * Inner class to represent different aspect ratios.
	 * 
	 * @author Dominick Leppich
	 *
	 */
	static class AspectRatio {
		private int width;
		private int height;

		public AspectRatio(int width, int height) {
			this.width = width;
			this.height = height;
		}

		public String toString() {
			return width + "x" + height;
		}
	}

	// ------------------------------------------------------------
	/* Some constant aspect ratios */

	public static final AspectRatio RATIO_CUSTOM = null;
	public static final AspectRatio RATIO_3_2 = new AspectRatio(3, 2);
	public static final AspectRatio RATIO_4_3 = new AspectRatio(4, 3);
	public static final AspectRatio RATIO_5_4 = new AspectRatio(5, 4);
	public static final AspectRatio RATIO_8_5 = new AspectRatio(8, 5);
	public static final AspectRatio RATIO_16_9 = new AspectRatio(16, 9);

	public static final int[] DEFAULT_WIDTHS = { 160, 320, 640, 800, 1280, 1600, 1920, 2048, 2800, 3200, 3440, 3840,
			4096 }; // ...

	private static final long serialVersionUID = 1L;

	// ------------------------------------------------------------

	/**
	 * Return an array of default {@link RenderDimension}s.
	 * 
	 * @return Array of default {@link RenderDimension}s
	 */
	public static RenderDimension[] getDefaultRenderDimensions() {
		List<RenderDimension> list = new ArrayList<RenderDimension>();

		// Calculate default dimensions
		for (int i = 0; i < DEFAULT_WIDTHS.length; i++) {
			// 3 to 2
			list.add(new RenderDimension(DEFAULT_WIDTHS[i], DEFAULT_WIDTHS[i] * 2 / 3));
			// 4 to 3
			list.add(new RenderDimension(DEFAULT_WIDTHS[i], DEFAULT_WIDTHS[i] * 3 / 4));
			// 5 to 4
			list.add(new RenderDimension(DEFAULT_WIDTHS[i], DEFAULT_WIDTHS[i] * 4 / 5));
			// 8 to 5
			list.add(new RenderDimension(DEFAULT_WIDTHS[i], DEFAULT_WIDTHS[i] * 5 / 8));
			// 16 to 9
			list.add(new RenderDimension(DEFAULT_WIDTHS[i], DEFAULT_WIDTHS[i] * 9 / 16));
		}

		RenderDimension[] dimensions = new RenderDimension[list.size()];
		list.toArray(dimensions);
		return dimensions;
	}

	// ------------------------------------------------------------

	/**
	 * Ctr with width and height.
	 * 
	 * @param width
	 *            - Width
	 * @param height
	 *            - Height
	 */
	public RenderDimension(int width, int height) {
		super(width, height);
	}

	/**
	 * Get the aspect ratio value (width divided by height).
	 * 
	 * @return The aspect ratio value
	 */
	public double getAspectRatioValue() {
		return (double) getWidth() / getHeight();
	}

	/**
	 * Get the aspect ratio (one of the static final members of
	 * {@link RenderDimension}.
	 * 
	 * @return The aspect ratio
	 */
	public AspectRatio getAspectRatio() {
		double ratio = getAspectRatioValue();
		if (ratio == 3.0 / 2)
			return RATIO_3_2;
		if (ratio == 4.0 / 3)
			return RATIO_4_3;
		if (ratio == 5.0 / 4)
			return RATIO_5_4;
		if (ratio == 8.0 / 5)
			return RATIO_8_5;
		if (ratio == 16.0 / 9)
			return RATIO_16_9;
		return RATIO_CUSTOM;
	}

	// ------------------------------------------------------------
	
	/**
	 * Returns a readable and user friendly String representation.
	 * 
	 * @return String representation
	 */
	public String toString() {
		AspectRatio ratio = getAspectRatio();
		return (int) getWidth() + " x " + (int) getHeight() + (ratio != null ? " [" + ratio + "]" : "");
	}
}
