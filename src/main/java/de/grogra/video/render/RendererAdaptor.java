package de.grogra.video.render;

import java.awt.Dimension;
import java.awt.Image;
import java.io.IOException;

import de.grogra.graph.GraphState;
import de.grogra.imp.Renderer;
import de.grogra.imp3d.View3D;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.expr.Expression;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.StringMap;
import de.grogra.util.ThreadContext;
import de.grogra.video.model.VideoImage;

/**
 * Creates an image using a {@link de.grogra.imp.Renderer Renderer} instance.
 * This class adapts the GroIMP renderer to the abstract {@link ImageProvider}
 * class.
 * 
 * @author Dominick Leppich
 *
 */
public class RendererAdaptor extends ImageProvider {
	private Workbench workbench;
	private Item item;
	private final String name;

	// ------------------------------------------------------------

	/**
	 * Create an adaptor for GroIMP renderer. In order to work this adaptor needs a
	 * reference to the projects {@link Workbench} and the {@link Item} representing
	 * the {@link de.grogra.imp.Renderer Renderer} to use.
	 * 
	 * @param workbench
	 *            - GroIMP {@link Workbench}
	 * @param item
	 *            - GroIMP Registry {@link Item} representing the renderer
	 */
	public RendererAdaptor(Workbench workbench, Item item) {
		this.workbench = workbench;
		this.item = item;
		this.name = ((Renderer) ((Expression) item).evaluate(workbench, new StringMap())).getName();
	}

	// ------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 * 
	 * The image is created using a {@link de.grogra.imp.Renderer Renderer}
	 * instance.
	 * 
	 * @throws InterruptedException
	 *             if the image creation is interrupted
	 * @throws IOException
	 *             if creating a {@link VideoImage} fails
	 */
	@Override
	public VideoImage createImage(Dimension dimension) throws IOException, InterruptedException {
		// Casting cannot fail at this stage
		Renderer renderer = (Renderer) ((Expression) item).evaluate(workbench, new StringMap());

		View3D view = View3D.getDefaultView(workbench);

		renderer.initialize(view, dimension.width, dimension.height);

		GraphState state;
//		private RGGGraph extent;
			Registry.setCurrent (workbench);
			Workbench.setCurrent (workbench);
			ThreadContext c = ThreadContext.current ();
			ThreadContext wc = workbench.getJobManager ().getThreadContext (); 
			c.setPriority (ThreadContext.MAX_PRIORITY);
			state = GraphState.get (workbench.getRegistry ().getProjectGraph (), wc).forContext (c);
//			extent = Runtime.INSTANCE.currentGraph ();
//			extent.derive ();
	
			Image renderedImage = null;
		try
		{
			renderedImage = renderer.computeImage();
		}
		finally
		{
			Registry.setCurrent (null);
			Workbench.setCurrent (null);
			state.dispose ();
			state = null;
		}

		return new VideoImage(renderedImage);
	}

	@Override
	public String getName() {
		// Flux has no name..
		if (name == null)
			return "Flux";
		else
			return name;
	}
}
