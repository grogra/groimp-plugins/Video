package de.grogra.video.render;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import de.grogra.imp3d.View3D;
import de.grogra.pf.ui.Workbench;
import de.grogra.video.VideoPlugin;
import de.grogra.video.model.VideoImage;
import de.grogra.xl.lang.ObjectConsumer;

/**
 * This class is able to make AWT and OpenGL snapshots of the GroIMP scene.
 * 
 * @author Dominick Leppich
 *
 */
public class SceneSnapshotMaker extends ImageProvider {
	/**
	 * Current GroIMP workbench
	 */
	private Workbench workbench;
	/**
	 * Temporary variable to hold the snapshot image
	 */
	private BufferedImage snapshotImage;
	/**
	 * Callback function to process snapshot image
	 */
	private SceneConsumer imageConsumer;
	/**
	 * We need to temporary save the render dimension, to access it in the
	 * {@link ObjectConsumer} method.
	 */
	private Dimension renderDimension;
	/**
	 * The dimension of the snapshots is saved temporary in order to register
	 * changes
	 */
	private Dimension snapshotDimension;

	/**
	 * {@link ReentrantLock} to synchronize callback and
	 * {@link #createImage(Dimension)} method
	 */
	private Lock consumerLock = new ReentrantLock();
	/**
	 * {@link Condition} to synchronize callback and {@link #createImage(Dimension)}
	 * method
	 */
	private Condition consumerCondition = consumerLock.newCondition();

	// ------------------------------------------------------------

	/**
	 * The {@code SceneConsumer} can be passed to the
	 * {@link de.grogra.imp.ViewComponent#makeSnapshot(ObjectConsumer)
	 * makeSnapshot(ObjectConsumer)} method to process the snapshot image. This
	 * implementation of the {@link de.grogra.xl.lang.ObjectConsumer ObjectConsumer}
	 * interface scales the image to the desired resolution and saves it as a member
	 * variable of the {@link SceneSnapshotMaker}.
	 * 
	 * @author Dominick Leppich
	 *
	 */
	private class SceneConsumer implements ObjectConsumer<Object> {
		public AtomicBoolean ready = new AtomicBoolean();

		@Override
		public void consume(Object value) {
			try {
				consumerLock.lock();

				// Clear image
				snapshotImage = null;

				BufferedImage image = (BufferedImage) value;

				// Check if the dimension of the scene in GroIMP changed. This can lead to wrong
				// scaling of the resulting image. The user gets a warning message.
				if (snapshotDimension == null)
					snapshotDimension = extractDimension(image);
				if (!snapshotDimension.equals(extractDimension(image))) {
					snapshotDimension = extractDimension(image);
					VideoPlugin.showWarning("Scene dimension changed!");
				}

				int rw = (int) renderDimension.getWidth();
				int rh = (int) renderDimension.getHeight();

				int w = image.getWidth(null);
				int h = image.getHeight(null);

				double ratio = (double) w / h;

				w = rw;
				h = (int) (w / ratio);

				// Check if height exceeds the limit
				if (h > rh) {
					h = (int) rh;
					w = (int) (h * ratio);
				}

				int x = (rw - w) / 2;
				int y = (rh - h) / 2;

				// Create buffered image to draw snapshot on
				snapshotImage = new BufferedImage(rw, rh, BufferedImage.TYPE_INT_RGB);

				Graphics g = snapshotImage.getGraphics();

				// Fill background with awt gray color
				g.setColor(new Color(204, 204, 204));
				g.fillRect(0, 0, rw, rh);

				// Draw scaled snapshot image
				g.drawImage(image, x, y, w, h, null);
			} finally {
				ready.set(true);
				consumerCondition.signalAll();
				consumerLock.unlock();
			}
		}
	}

	// ------------------------------------------------------------

	/**
	 * Create a new SceneSnapshotMaker to the current workbench
	 * 
	 * @param workbench
	 *            GroIMP workbench
	 */
	public SceneSnapshotMaker(Workbench workbench) {
		this.workbench = workbench;

		// Create a callback object
		imageConsumer = new SceneConsumer();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param dimension
	 *            the dimension the awt snapshot will be fit in
	 * @return AWT Snapshot image
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws OutOfMemoryException
	 */
	@Override
	public VideoImage createImage(Dimension dimension) throws IOException, InterruptedException {
		renderDimension = dimension;

		// Get the view and call the corresponding makeSnapshot method
		View3D view = View3D.getDefaultView(workbench);

		// Make sure, this code is processed in the correct order using Locks and
		// Conditions. Before the "snapshotImage" member can be used, the "consume"
		// method needs to be called and processed.
		try {
			imageConsumer.ready.set(false);
			consumerLock.lock();
			view.getViewComponent().makeSnapshot(imageConsumer);
			if (!imageConsumer.ready.get()) {
				consumerCondition.await();
			}
		} finally {
			consumerLock.unlock();
		}

		if (snapshotImage == null)
			throw new IOException("SceneSnapshot image failed");

		// The snapshotImage has been filled with content in the consumer method and can
		// be returned now
		return new VideoImage(snapshotImage);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return "SceneSnapshot";
	}

	// ------------------------------------------------------------

	/**
	 * Get the {@link Dimension} of an image.
	 * 
	 * @param image
	 *            - {@link Image}
	 * @return {@link Dimension} of this image
	 */
	public static Dimension extractDimension(Image image) {
		return new Dimension(image.getWidth(null), image.getHeight(null));
	}
}
