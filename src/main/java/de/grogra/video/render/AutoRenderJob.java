package de.grogra.video.render;

import java.awt.Dimension;
import java.io.IOException;

import de.grogra.video.model.ImageSequenceControl;
import de.grogra.video.simulation.SimulationMethod;
import de.grogra.video.util.Job;
import de.grogra.video.util.Progress;
import de.grogra.video.util.Worker;

/**
 * The {@code AutoRenderJob} encapsulates the automatic simulation and rendering
 * of images into a {@link Job} in order to be processed by a {@link Worker}.
 * 
 * @author Dominick Leppich
 *
 */
public class AutoRenderJob extends Job {
	private ImageProvider provider;
	private Dimension dimension;
	private SimulationMethod method;
	private ImageSequenceControl control;
	private int n;

	// ------------------------------------------------------------

	/**
	 * Ctr.
	 * 
	 * @param provider
	 *            - {@link ImageProvider} (Source of image creation)
	 * @param dimension
	 *            - {@link Dimension} of the image
	 * @param method
	 *            - {@link SimulationMethod} to be called before rendering
	 * @param control
	 *            - {@link ImageSequenceControl} to the {@link ImageSequence}, where
	 *            the images will be added
	 * @param n
	 *            - Times this job repeats
	 */
	public AutoRenderJob(ImageProvider provider, Dimension dimension, SimulationMethod method,
			ImageSequenceControl control, int n) {
		this.provider = provider;
		this.dimension = dimension;
		this.method = method;
		this.control = control;
		this.n = n;
	}

	// ------------------------------------------------------------

	@Override
	public void process() throws IOException, InterruptedException {
		setProgress(new Progress(0.0,
				"Auto-Render: (" + provider.getName() + ", " + dimension + ", " + method.getName() + ")"));
		for (int i = 0; i < n; i++) {
			// Call method
			method.execute();
			setProgress(new Progress((double) (2 * i + 1) / (2 * n),
					"Auto-Render: (" + provider.getName() + ", " + dimension + ", " + method.getName() + ")"));

			// Render
			control.add(provider.createImage(dimension));
			setProgress(new Progress((double) (2 * i + 2) / (2 * n),
					"Auto-Render: (" + provider.getName() + ", " + dimension + ", " + method.getName() + ")"));
		}
	}
}
