/**
 * This package contains all classes and interfaces which belong to the video
 * creation process.
 * 
 * @author Dominick Leppich
 * 
 */
package de.grogra.video.export;