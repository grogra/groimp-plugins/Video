package de.grogra.video.export;

/**
 * This class collects all information which are relevant in order to create a
 * video.
 * 
 * @author Dominick Leppich
 *
 */
public class VideoSettings {
	public static final int FPS_MIN = 1;
	public static final int FPS_MAX = 120;
	public static final int FPS_INIT = 25;
	public static final int OUTPUT_FPS_MIN = 10;

	private int inputFps;
	private int outputFps;

	// ------------------------------------------------------------

	/**
	 * Default ctr.
	 */
	public VideoSettings() {
		this(FPS_INIT, FPS_INIT);
	}

	/**
	 * Ctr defining input and output fps.
	 * 
	 * @param inputFps
	 *            - Input fps
	 * @param outputFps
	 *            - Output fps
	 */
	public VideoSettings(int inputFps, int outputFps) {
		setInputFps(inputFps);
		setOutputFps(outputFps);
	}

	// ------------------------------------------------------------

	public int getInputFps() {
		return inputFps;
	}

	public void setInputFps(int inputFps) {
		if (inputFps < FPS_MIN || inputFps > FPS_MAX)
			throw new IllegalArgumentException("illegal input fps: " + inputFps);
		this.inputFps = inputFps;
	}

	public int getOutputFps() {
		return outputFps;
	}

	public void setOutputFps(int outputFps) {
		if (outputFps < OUTPUT_FPS_MIN || outputFps > FPS_MAX)
			throw new IllegalArgumentException("illegal output fps: " + outputFps);
		this.outputFps = outputFps;
	}
}
