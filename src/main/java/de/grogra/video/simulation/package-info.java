/**
 * This package contains the logic to change the scene to be rendered
 * automatically.
 * 
 * @author Dominick Leppich
 * 
 */
package de.grogra.video.simulation;