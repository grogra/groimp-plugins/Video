package de.grogra.video.simulation;

import de.grogra.pf.ui.Workbench;
import de.grogra.reflect.Method;
import de.grogra.rgg.RGG;

/**
 * A {@code RGGMethodAdaptor} adapts GroIMP {@link Method} objects to the
 * abstract {@link SimulationMethod} class.
 * 
 * @author Dominick Leppich
 *
 */
public class RGGMethodAdaptor extends SimulationMethod {
	private Method method;
	private Workbench workbench;
	private RGG rgg;

	// ------------------------------------------------------------

	/**
	 * Ctr.
	 * 
	 * @param method
	 *            - RGG method object
	 */
	public RGGMethodAdaptor(Workbench workbench, RGG rgg, Method method) {
		this.method = method;
		this.workbench = workbench;
		this.rgg = rgg;
	}

	// ------------------------------------------------------------

	@Override
	public void execute() {
		// Create a new Apply object and run it. GroIMP uses these Apply objects as well
		// for method invocation.
		RGG.Apply apply = rgg.new Apply(method, false, false);
		apply.run(workbench, workbench);
		try
		{
			while (!apply.isDone())
				synchronized (apply) {
				apply.wait();
				}
		}
		catch (InterruptedException e)
		{
			
		}
	}

	@Override
	public String getName() {
		return method.getSimpleName();
	}
}
