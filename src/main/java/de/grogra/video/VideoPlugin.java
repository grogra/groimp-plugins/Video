package de.grogra.video;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.swing.JOptionPane;

import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.swing.WindowSupport;
import de.grogra.util.Map;
import de.grogra.video.connector.GroIMPConnector;
import de.grogra.video.connector.VideoPluginConnector;
import de.grogra.video.export.FFmpegWrapper;
import de.grogra.video.export.VideoExporter;
import de.grogra.video.interpolation.AlphaFadingStrategy;
import de.grogra.video.interpolation.InterpolationStrategy;
import de.grogra.video.ui.VideoPanel;
import de.grogra.video.ui.VideoPanelWrapper;

/**
 * The {@code VideoPlugin} class connects the video export plugin with the rest
 * of the GroIMP software.
 * 
 * It provides easy access to default implementations of
 * {@link VideoPluginConnector} and {@link VideoExporter} objects.
 * 
 * Additionally the static method {@link #getInterpolationStrategies()} can be
 * used to get a list of available {@link InterpolationStrategy}s.
 * 
 * The method to create the export panel, throw the callback in the GroIMP
 * registry, is located in this class as well.
 * 
 * Last but not least there are methods for central error handling.
 * 
 * @author Dominick Leppich
 *
 */
public class VideoPlugin {
	/** An executor object to show error or warning messages */
	private static final Executor executor;

	// ------------------------------------------------------------

	static {
		executor = Executors.newSingleThreadExecutor();
	}

	// ------------------------------------------------------------

	/**
	 * Returns an instance of the default {@link VideoPluginConnector}
	 * implementation
	 * 
	 * @param workbench
	 *            - Workbench
	 * 
	 * @return A {@link VideoPluginConnector} instance
	 */
	public static VideoPluginConnector createDefaultConnector(Workbench workbench) {
		return new GroIMPConnector(workbench);
		// return new TestConnector(new File("SOME_PATH"));
	}

	/**
	 * Returns the default {@link VideoExporter} implementation
	 * 
	 * @return {@link VideoExporter}
	 */
	public static VideoExporter createDefaultExporter() {
		return new FFmpegWrapper();
		// return new TestExporter();
	}

	/**
	 * Get a list of all available {@link InterpolationStrategy} objects. So far
	 * there is only one strategy, the {@link AlphaFadingStrategy} implemented.
	 * 
	 * @return A list of all {@link InterpolationStrategy} objects
	 */
	public static List<InterpolationStrategy> getInterpolationStrategies() {
		List<InterpolationStrategy> list = new ArrayList<>();
		list.add(new AlphaFadingStrategy());
		return list;
	}

	// ------------------------------------------------------------

	/**
	 * Create a new video export panel. This is the entry point of the plugin panel.
	 * 
	 * @param ctx
	 *            - GroIMP {@link Context}
	 * @param params
	 *            - A {@link Map} of parameters
	 * @return An instance of the video export panel, wrapped in a GroIMP
	 *         {@link Panel}
	 */
	public static Panel createVideoPanel(Context ctx, Map params) {
		return new VideoPanelWrapper(new VideoPanel(createDefaultConnector(ctx.getWorkbench())))
				.initialize((WindowSupport) ctx.getWindow(), params);
	}

	// ------------------------------------------------------------

	/**
	 * Handles a {@link Throwable} object (an error). The method
	 * {@link #handleError(Component, Throwable)} is used for the concrete handling.
	 * 
	 * @param e
	 *            - {@link Throwable}
	 */
	public static void handleError(Throwable e) {
		handleError(null, e);
	}

	/**
	 * Handles a {@link Throwable} object (an error) with a given {@link Component}
	 * (the caller). The error is printed to the error stream and shown in an error
	 * dialog using the {@link JOptionPane} class.
	 * 
	 * @param caller
	 *            - The calling {@link Component} object
	 * @param e
	 *            - {@link Throwable}
	 */
	public static void handleError(final Component caller, final Throwable e) {
		e.printStackTrace(System.err);
		executor.execute(new Runnable() {
			@Override
			public void run() {
				JOptionPane.showMessageDialog(caller, "An error occured: \n\n" + e.getMessage(), "VideoPlugin",
						JOptionPane.ERROR_MESSAGE);
			}
		});
	}

	/**
	 * Show a warning message to the user, using the {@link JOptionPane} class.
	 * 
	 * @param message
	 *            - The message
	 */
	public static void showWarning(final String message) {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				JOptionPane.showMessageDialog(null, message, "VideoPlugin", JOptionPane.WARNING_MESSAGE);
			}
		});
	}
}
